echo "Processing assets"

mkdir -p public/fonts public/img
cp node_modules/bootstrap/fonts/*.{ttf,woff,eot,svg,woff2} node_modules/font-awesome/fonts/*.{ttf,woff,eot,svg,woff2,otf} fonts/*.{ttf,woff,woff2} node_modules/bootcards/dist/fonts/*.{ttf,woff,eot,svg,woff2,otf} public/fonts
cp node_modules/leaflet/dist/images/** img/** public/img

npx pug pug/pages --out public

echo "Processing events"

mkdir -p public/events
echo "events:" > public/events/index.yaml
for f in $(ls -r events/details); do
    event=$(basename $f .yaml)
    echo "Processing $event"
    echo "  $event:" >> public/events/index.yaml
    cat "events/details/$f" | sed 's/^/    /' >> public/events/index.yaml
    if ! [ -f public/events/$event.html ]; then
        npx js-yaml "events/details/$f" > "public/events/$event.json"
        npx pug -O public/events/$event.json pug/templates/individual_event_page.pug
        mv pug/templates/individual_event_page.html public/events/$event.html
    fi
done

npx js-yaml public/events/index.yaml > public/events/index.json

npx pug -O public/events/index.json pug/templates/events.pug

mv pug/templates/events.html public/events/index.html


echo "Processing CSS"

mkdir -p public/css
cat 'node_modules/bootstrap/dist/css/bootstrap.min.css' 'node_modules/font-awesome/css/font-awesome.min.css' 'node_modules/leaflet/dist/leaflet.css' 'node_modules/bootcards/dist/css/bootcards-desktop.min.css' css/*.css | npx postcss > public/css/index.css

echo "Processing JS"
mkdir -p public/js
npx uglify-js 'node_modules/jquery/dist/jquery.min.js' 'node_modules/bootstrap/dist/js/bootstrap.min.js' 'node_modules/leaflet/dist/leaflet.js' 'node_modules/bootcards/dist/js/bootcards.min.js' 'js/*.js' -o public/js/index.js