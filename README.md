## Official Website of Free Software User Groups in India

## Dependencies

1. Node

## Presteps

1. Install Node ( Welcome to the world of Node <3 )

## Installation

1. `npm install` from root folder


## Building

1. `npm run build`

## License

Official Website of Free Software User Groups in India  
Copyleft (ɔ) 2017 FSCI (Free Software Community of India) - All rights reserved, all wrongs reversed   

This project comes under free software license: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.  

This project is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
