/*
  a well structured JSON, containing the following fields:
    1. LAT: mandatory
    2. LNG: mandatory
    3. TITLE: mandatory
    4. URL: optional
    5. MLIST: optional
    6. MATURL: optional
    7. TGURL: optional
    (have I missed something?)
 */
var locations = [{
  "LAT": "9.918897",
  "LNG": "77.10249",
  "TITLE": "FSUG (Free Software User Group) Idukki",
  "URL": "https://groups.google.com/group/fsug-idk",
  "MLIST": "groups.google.com/group/fsug-idk",
  "MATURL": "https://matrix.to/#/#fsug-idk:matrix.org",
  "TGURL": ""
}, {
  "LAT": "17.4359194",
  "LNG": "78.3604184",
  "TITLE": "Swecha",
  "URL": "http://www.swecha.org/",
  "MLIST": "http://swecha.net/mailman/listinfo",
  "MATURL": "https://matrix.to/#/#swecha:matrix.org",
  "TGURL": ""
}, {
  "LAT": "12.9473557",
  "LNG": "77.5996612",
  "TITLE": "FSMK (Free Software Movement Karnataka)",
  "URL": "https://fsmk.org/",
  "MLIST": "https://discuss.fsmk.org/",
  "MATURL": "https://matrix.to/#/#fsmk-discuss:matrix.org",
  "TGURL": "https://telegram.me/joinchat/AZWi6jvE1RV2eckTYg4YGg"
}, {
  "LAT": "12.501389",
  "LNG": "74.99",
  "TITLE": "Kasargod FSUG (Free Software User Group)",
  "URL": "https://groups.google.com/group/fsug-ksd",
  "MLIST": "https://groups.google.com/group/fsug-ksd",
  "MATURL": "https://matrix.to/#/#fsug-ksd:matrix.org",
  "TGURL": ""
}, {
  "LAT": "23.483333",
  "LNG": "87.316667",
  "TITLE": "dgplug (Linux Users' Group of Durgapur)",
  "URL": "https://www.dgplug.org/",
  "MLIST": "http://lists.dgplug.org/listinfo.cgi/users-dgplug.org",
  "MATURL": "https://matrix.to/#/#dgplug:libera.chat",
  "TGURL": ""
}, {
  "LAT": "18.975",
  "LNG": "72.825833",
  "TITLE": "ILUG (Indian Libre Software user Group) Bombay",
  "URL": "http://www.ilug-bom.org.in/",
  "MLIST": "http://mm.ilug-bom.org.in/mailman/listinfo/linuxers",
  "MATURL": "https://matrix.to/#/#ilug-bom:matrix.org",
  "TGURL": ""
}, {
  "LAT": "11.605",
  "LNG": "76.083",
  "TITLE": "FSUG (Free Software User Group) Wayanad",
  "URL": "http://wayanad.fsug.in",
  "MLIST": "https://groups.google.com/group/fsug-wayanad",
  "MATURL": "https://matrix.to/#/#fsug-wayanad:matrix.org",
  "TGURL": "https://t.me/fsug_wayanad"
}, {
  "LAT": "9.49",
  "LNG": "76.33",
  "TITLE": "ILUG (Indian Libre Software User Group) Alleppey",
  "URL": "http://alpy.ilug.org.in",
  "MLIST": "https://groups.google.com/group/ilug-alpy",
  "MATURL": "https://matrix.to/#/#ilug-alpy:diasp.in",
  "TGURL": "https://t.me/ilug_alpy"
}, {
  "LAT": "11.8689",
  "LNG": "75.3555",
  "TITLE": "FSUG (Free Software User Group) Kannur",
  "URL": "https://groups.google.com/group/fsugknr",
  "MLIST": "https://groups.google.com/group/fsugknr",
  "MATURL": "https://matrix.to/#/#fsugknr:matrix.org",
  "TGURL": "https://t.me/fsugknr"
}, {
  "LAT": "8.88",
  "LNG": "76.60",
  "TITLE": "SSUG (Swathanthra Software Users Group) Kollam",
  "URL": "http://kollam.fsug.in",
  "MLIST": "https://www.freelists.org/list/ssug-kollam",
  "MATURL": "https://matrix.to/#/#ssug-kollam:matrix.org",
  "TGURL": "https://t.me/ssugk"
}, {
  "LAT": "",
  "LNG": "",
  "TITLE": "ILUG (Indian Libre Software User Group) Kottayam",
  "URL": "",
  "MLIST": "",
  "MATURL": "https://matrix.to/#/#ilug-ktm:matrix.org",
  "TGURL": "https://t.me/ilugkottayam"
}, {
  "LAT": "11.25",
  "LNG": "75.78",
  "TITLE": "FSUG (Free Software User Group) Calicut",
  "URL": "http://calicut.fsug.in",
  "MLIST": "https://www.freelists.org/list/fsug-calicut",
  "MATURL": "https://matrix.to/#/#fsug-calicut:matrix.org",
  "TGURL": "https://t.me/fsugcalicut"
}, {
  "LAT": "11.04",
  "LNG": "76.08",
  "TITLE": "SSUG (Swathanthra Software Users Group) Malappuram",
  "URL": "http://ssug.ml",
  "ORL": "http://kl10.fsug.in",
  "MLIST": "https://www.freelists.org/list/ssug-malappuram",
  "MATURL": "https://matrix.to/#/#ssugm:diasp.in",
  "TGURL": "https://t.me/ssugm"
}, {
  "LAT": "10.77",
  "LNG": "76.65",
  "TITLE": "PLUS (Palakkad Libre software Users Society)",
  "URL": "http://plus.fosscommunity.in",
  "MLIST": "https://www.loomio.org/g/p9JCX308/plus",
  "MATURL": "https://matrix.to/#/#plus:matrix.org",
  "TGURL": "https://t.me/PlusFreedom"
}, {
  "LAT": "8.49",
  "LNG": "76.95",
  "TITLE": "FSUG (Free Software User Group) Thiruvananthapuram",
  "URL": "https://tvm.fsug.in",
  "MLIST": "https://groups.google.com/d/forum/ilug-tvm",
  "MATURL": "https://matrix.to/#/#fsug-tvm:matrix.org",
  "TGURL": "https://t.me/fsugtvm"
}, {
  "LAT": "10.52",
  "LNG": "76.21",
  "TITLE": "FSUG (Free Software User Group) Thrissur",
  "URL": "http://thrissur.fsug.in",
  "MLIST": "http://www.freelists.org/list/fsug-thrissur",
  "MATURL": "https://matrix.to/#/#fsug-tcr:matrix.org",
  "TGURL": "https://t.me/fsugtcr"
}, {
  "LAT": "9.97",
  "LNG": "76.28",
  "TITLE": "ILUG (Indian Libre Software User Group) Cochin",
  "URL": "http://ilugcoch.in",
  "MLIST": "https://www.loomio.org/g/UPxFGw8I/ilugcochin",
  "MATURL": "https://matrix.to/#/#ilugcochin:matrix.org",
  "TGURL": "https://t.me/ilugcochin"
}, {
  "LAT": "10.00",
  "LNG": "76.33",
  "TITLE": "FOSSClub, Ernakulam",
  "URL": "http://fossclub.in",
  "MLIST": "",
  "MATURL": "",
  "TGURL": ""
}, {
  "LAT": "10.01",
  "LNG": "76.34",
  "TITLE": "RSETFC (Rajagiri School of Engineering & Technology FOSS Club), Ernakulam",
  "URL": "",
  "MLIST": "",
  "MATURL": "https://matrix.to/#/#rsetfc:matrix.org",
  "TGURL": "https://t.me/rsetfossclub"
}, {
  "LAT": "12.50",
  "LNG": "75.05",
  "TITLE": "FSUG (Free Software User Group) at LBS College of Engineering, Kasargode",
  "URL": "http://lbs.fsug.in/",
  "MLIST": "https://groups.google.com/group/fsug-lbs",
  "MATURL": "",
  "TGURL": ""
}, {
  "LAT": "10.84",
  "LNG": "76.03",
  "TITLE": "FSUG (Free Software User Group) at MES College of Engineering, Malappuram",
  "URL": "https://groups.google.com/group/mes-fsug",
  "MLIST": "https://groups.google.com/forum/#!forum/mes-fsug",
  "MATURL": "",
  "TGURL": ""
}, {
  "LAT": "18.5293994",
  "LNG": "73.8560156",
  "TITLE": "FSUG (Free Software User Group) at College of Engineering, Pune",
  "URL": "http://co.fsug.in/",
  "MLIST": "https://groups.google.com/forum/#!forum/cofsug",
  "MATURL": "https://matrix.to/#/#cofsug:disroot.org",
  "TGURL": ""
}, {
  "LAT": "15.49",
  "LNG": "73.82",
  "TITLE": "FSUG (Free Software User Group) Goa",
  "URL": "http://goa.fsug.in",
  "MLIST": "https://groups.yahoo.com/group/fsug-goa",
  "MATURL": "https://matrix.to/#/#fsug-goa:matrix.org",
  "TGURL": ""
}, {
  "LAT": "18.519368",
  "LNG": "73.855321",
  "TITLE": "Pune GNU/Linux Users Group, Pune",
  "URL": "http://plug.org.in",
  "MLIST": "",
  "MATURL": "",
  "TGURL": ""
}, {
  "LAT": "28.99",
  "LNG": "77.7",
  "TITLE": "GLUG (Gnu/Linux Users Group) Meerut",
  "MLIST": "https://groups.google.com/forum/#!forum/glug-meerut",
  "MATURL": "",
  "TGURL": ""
}, {
  "LAT": "28.7140",
  "LNG": "77.3000",
  "TITLE": "NDBUG (New Delhi BSD User Group)",
  "URL": "http://ndbug.in",
  "MLIST": "",
  "MATURL": "",
  "TGURL": ""
}, {
  "LAT": "11.3217",
  "LNG": "75.9342",
  "TITLE": "NIT Calicut FOSS Cell",
  "URL": "http://fosscell.nitc.ac.in",
  "MLIST": "https://groups.google.com/forum/#!forum/nitcosc",
  "MATURL": "https://matrix.to/#/#fosscell:diasp.in",
  "TGURL": "https://t.me/fosscell"
}, {
  "LAT": "28.6139",
  "LNG": "77.2090",
  "TITLE": "Indian Linux User's Group Delhi",
  "URL": "http://www.linuxdelhi.org/",
  "MLIST": "",
  "MATURL": "https://matrix.to/#/#ilugd:matrix.org",
  "TGURL": "https://t.me/joinchat/AAAAAEAc48wCHkUXViXBcg"
}, {
   "LAT":" 23.6",
   "LNG": "72.95",
   "TITLE": "UnitedBSD (BSD users Group) ",
   "URL": "https://unitedbsd.com",
   "MLIST": "",
   "MATURL": "https://riot.im/app/#/room/#bsd:matrix.org",
   "TGURL": "https://t.me/unitedbsd"
}, {
   "LAT":" 10.62655",
   "LNG": "76.14496",
   "TITLE": "FOSSers VAST",
   "URL": "http://fossers.vidyaacademy.ac.in",
   "MLIST": "https://lists.fsci.org.in/postorius/lists/fossersvast.lists.fsci.org.in",
   "MATURL": "https://matrix.to/#/#FOSSersVAST:matrix.org",
   "TGURL": "https://t.me/fossersvast"
}, {
   "LAT":"11.93855",
   "LNG": "79.4984801",
   "TITLE": "Viluppuram GLUG",
   "URL": "https://vglug.org",
   "MLIST": "https://groups.google.com/forum/#!forum/viluppuramglug",
   "MATURL": "https://matrix.to/#/+vglug:matrix.org",
   "TGURL": ""
}, {
  "LAT": "28.6129",
  "LNG": "77.2277",
  "TITLE": "PyDelhi",
  "URL": "https://pydelhi.org/",
  "MLIST": "https://mail.python.org/mailman/listinfo/ncr-python.in",
}, {
  "LAT": "28.6328",
  "LNG": "77.2195",
  "TITLE": "Mozilla Delhi",
  "URL": "https://wiki.mozilla.org/India/Delhi",
  "TGURL": "https://t.me/mozilladelhi",
}, {
  "LAT": "28.4653",
  "LNG": "77.5106",
  "TITLE": "Greater Noida Developer Group",
  "URL": "http://www.gndg.in/",
  "TGURL": "https://t.me/joinchat/AoNm4T5dyUMR40KBgwuF8w",
}, {
   "LAT":"9.727128",
   "LNG": "76.726013",
   "TITLE": "The Nexus, SJCET Palai (Free Software and Hardware Users Group/Club) ",
   "URL": "https://github.com/thenexusproject",
   "MLIST": "",
   "MATURL": "",
   "TGURL": ""
}, {
  "LAT":"12.991829015583626",
  "LNG": "80.22862809291958",
  "TITLE": "ILUGC (Indian Linux User Group - Chennai)",
  "URL": "https://ilugc.in",
  "MLIST": "https://www.freelists.org/list/ilugc",
  "MATURL": "https://matrix.to/#/#ilugc:libera.chat",
  "TGURL": ""
}];